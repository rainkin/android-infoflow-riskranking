FROM marple/base
MAINTAINER Rainkin <rainkin1993@gmail.com>

ENV SOURCE .

# add source code
ADD $SOURCE /code

# dependences
RUN cp -f /ta3-api-bindings-java/tc-bbn-avro/target/tc-avro-1.0-SNAPSHOT-jar-with-dependencies.jar /code/lib
RUN cp -f /ta3-serialization-schema/target/tc-schema-1.0-SNAPSHOT.jar /code/lib
RUN cp -f /ta3-serialization-schema/avro/TCCDMDatum.avsc /code/schema/TCCDMDatum.avsc

# entrypoint 
WORKDIR /code
RUN chmod +x startup.sh
ENTRYPOINT ["./startup.sh"]
