#!/bin/bash

# error automatically exit
set -e 

# branch selection
if  [ ! -n "$1" ] ;then
    branch="master"
    echo "pull from branch : $branch"
else
	echo "pull from branch : $1"
    branch="$1"
fi

git config --global user.email "you@example.com"
git config --global user.name "Your Name"
# update source code 
git pull -f origin "$branch"


# compile and run
javac -cp /code/lib/*:./src: src/edu/northwestern/marple/*.java

java -Xmx8g -cp  /code/lib/*:./src: edu.northwestern.marple.Main
