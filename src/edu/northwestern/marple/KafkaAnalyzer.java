package edu.northwestern.marple;

/*Principal, Subject, Event, NetflowObject, FileObject,SrcSinkObject,ProvenanceTagNode,MemoryObject,TagEntity,RegistryKeyObject,
		EVENT_ACCEPT, EVENT_BIND, EVENT_CHANGE_PRINCIPAL, EVENT_CHECK_FILE_ATTRIBUTES, EVENT_CLONE, EVENT_CLOSE, 
		EVENT_CONNECT, EVENT_CREATE_OBJECT, EVENT_CREATE_THREAD, EVENT_EXECUTE, EVENT_FORK, EVENT_LINK, EVENT_UNLINK, 
		EVENT_MMAP, EVENT_MODIFY_FILE_ATTRIBUTES, EVENT_MPROTECT, EVENT_OPEN, EVENT_READ, EVENT_RECVFROM, EVENT_RECVMSG, 
		EVENT_RENAME, EVENT_WRITE, EVENT_SIGNAL, EVENT_TRUNCATE, EVENT_WAIT, EVENT_OS_UNKNOWN, EVENT_KERNEL_UNKNOWN, 
		EVENT_APP_UNKNOWN, EVENT_UI_UNKNOWN, EVENT_UNKNOWN, EVENT_BLIND, EVENT_UNIT, EVENT_UPDATE, EVENT_SENDTO, EVENT_SENDMSG, 
		EVENT_SHM, EVENT_EXIT, 
		SUBJECT_PROCESS,SUBJECT_THREAD,SUBJECT_UNIT,SUBJECT_BASIC_BLOCK
		MATCH(f:EVENT_APP_UNKNOWN) WHERE f.uuid='00000000000000000000000019895559' return f;
		
		EDGE_EVENT_AFFECTS_MEMORY, EDGE_EVENT_AFFECTS_FILE, EDGE_EVENT_AFFECTS_NETFLOW, EDGE_EVENT_AFFECTS_SUBJECT, EDGE_EVENT_AFFECTS_SRCSINK, EDGE_EVENT_HASPARENT_EVENT, EDGE_EVENT_CAUSES_EVENT, EDGE_EVENT_ISGENERATEDBY_SUBJECT, EDGE_SUBJECT_AFFECTS_EVENT, EDGE_SUBJECT_HASPARENT_SUBJECT, EDGE_SUBJECT_HASLOCALPRINCIPAL, EDGE_SUBJECT_RUNSON, EDGE_FILE_AFFECTS_EVENT, EDGE_NETFLOW_AFFECTS_EVENT, EDGE_MEMORY_AFFECTS_EVENT, EDGE_SRCSINK_AFFECTS_EVENT, EDGE_OBJECT_PREV_VERSION, EDGE_FILE_HAS_TAG, EDGE_NETFLOW_HAS_TAG, EDGE_MEMORY_HAS_TAG, EDGE_SRCSINK_HAS_TAG, EDGE_SUBJECT_HAS_TAG, EDGE_EVENT_HAS_TAG, EDGE_EVENT_AFFECTS_REGISTRYKEY, EDGE_REGISTRYKEY_AFFECTS_EVENT, EDGE_REGISTRYKEY_HAS_TAG  

		*/

import java.nio.ByteBuffer;
import javax.xml.bind.DatatypeConverter;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.alg.*;
import org.jgrapht.*;
import org.jgrapht.graph.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericContainer;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import com.bbn.tc.schema.avro.Event;
import com.bbn.tc.schema.avro.Value;
import com.bbn.tc.schema.avro.ValueDataType;
import com.bbn.tc.schema.avro.NetFlowObject;
import com.bbn.tc.schema.avro.ProvenanceTagNode;

import com.bbn.tc.schema.avro.SimpleEdge;
import com.bbn.tc.schema.avro.SrcSinkObject;
import com.bbn.tc.schema.avro.Subject;
import com.bbn.tc.schema.avro.TCCDMDatum;
import com.bbn.tc.schema.avro.UUID;
import com.bbn.tc.schema.serialization.AvroConfig;

import com.bbn.tc.schema.avro.ConfidentialityTag;
import com.bbn.tc.schema.avro.EdgeType;
import com.bbn.tc.schema.avro.EventType;
import com.bbn.tc.schema.avro.FileObject;
import com.bbn.tc.schema.avro.IntegrityTag;
import com.bbn.tc.schema.avro.MemoryObject;
import com.bbn.tc.schema.avro.Principal;
import com.bbn.tc.schema.avro.RegistryKeyObject;
import com.bbn.tc.schema.avro.SHORT;
import com.bbn.tc.schema.avro.SubjectType;
import com.bbn.tc.schema.avro.TagEntity;
import com.bbn.tc.schema.avro.TagOpCode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import kafka.Kafka;

public class KafkaAnalyzer {

	/*
	 * configuration variables
	 */
	private String consumerServer;
	private String producerServer;
	private String writeTopic;
	private String readTopic;
	private String groupId;
	private boolean exiton0;
	private String schemaFilename;

	/*
	 * kafka consumer and producer
	 */
	private KafkaConsumer consumer;
	private Producer producer;

	/*
	 * global variables to store traces
	 */
	// stores all Subject nodes, and create index for uuid using hashmap : <uuid
	// of Subject, Subject>
	private HashMap<String, String> blkSubjectNodes = new HashMap<String, String>();
	private HashMap<String, Double> blkSubjectScore = new HashMap<String, Double>();

	// stores all Subject nodes, and create index for uuid using hashmap : <uuid
	// of Subject, Subject>
	private HashMap<String, String> whtSubjectNodes = new HashMap<String, String>();

	// stores all Event nodes, and create index for uuid using hashmap : <uuid
	// of Event, Event>
	private HashMap<String, String> suspEventNodes = new HashMap<String, String>();

	// Subject -> Event, 1:n, <uuid of Subject, ArrayList<uuid of Event>>
	private HashMap<String, ArrayList<String>> subject2events = new HashMap<String, ArrayList<String>>();

	// Event -> Subject, 1:1, <uuid of Event, uuid of Subject>
	private HashMap<String, String> event2subject = new HashMap<String, String>();

	// stores pending Events which haven't been linked to Subject, that is, no
	// EDGE_EVENT_ISGENERATEDBY_SUBJECT related to these Events found.
	private HashMap<String, String> pendingEventNodes = new HashMap<String, String>();

	// Subject -> Event, 1:n, <uuid of SrcSink, ArrayList<uuid of Event>>
	private HashMap<String, ArrayList<String>> srcsink2events = new HashMap<String, ArrayList<String>>();

	// Event -> Subject, 1:1, <uuid of Event, uuid of Sink>
	private HashMap<String, String> event2srcsink = new HashMap<String, String>();

	// sensitive SrcSinkObject, and create index for uuid using hashmap : <uuid
	// of SrcSinkObject, SrcSinkObject>
	private HashMap<String, String> sensitiveSrcSink = new HashMap<String, String>();
	private HashMap<String, String> sensitiveFile = new HashMap<String, String>();

	private HashMap<String, String> pendingSrcSinkObject = new HashMap<String, String>();

	// a set of white apps' name, <string of white app's name>
	private HashSet<String> WHITEAPP = new HashSet<String>();

	// a list of uuid of Subject, which inside the white app list, <uuid of
	// Subject>
	private HashSet<String> matchedWhiteApp = new HashSet<String>();

	private long currTimeStamp = 0;
	private PrintWriter report_writer = null;

	private DirectedGraph<String, DefaultEdge> provGraph = new DefaultDirectedGraph<String, DefaultEdge>(
			DefaultEdge.class);

	// subj and its input Prov Tags to it, subj and its output Prov Tags to it.
	private HashMap<String, ArrayList<String>> subj2inputTags = new HashMap<String, ArrayList<String>>();
	private HashMap<String, ArrayList<String>> subj2outputTags = new HashMap<String, ArrayList<String>>();

	private String malProvNodesSrc = "22592]:22579]:22581]:22581]:22582]:22562]:22565]:22594]:22593]:13531]"
			+ ":22575]:22570]:11168]:";
			//+ ":25067]:23562]:23563:]";
	private String malProvNodesSink = "00000000000000000000000000005a19:00000000000000000000000000005c09:"
			+ "000000000000000000000000000061e9:00000000000000000000000000005832:"
			+ "0000000000000000000000000000583f:"
			+ "000000000000000000000000000061f3:";
	private HashSet<String> malSRC = new HashSet<String>();
	private HashSet<String> malSINK = new HashSet<String>(); 
	

	/**
	 * load configuration file
	 * 
	 * @param configFile
	 *            path/to/configFile
	 * @throws IOException
	 */

	// from Faros
	/*
	 * public static ArrayList<String> getParmString(Event item) { String[]
	 * tmpList = item.get("parameters").toString().split("name\": ");
	 * ArrayList<String> parms = new ArrayList<>(); for (int i = 1; i <
	 * tmpList.length; i++) { parms.add(tmpList[i].split(",")[0]); } //
	 * System.out.println(parms); return parms; }
	 */

	private void addMalNodeToCheck(HashSet<String> malset, String malString) {
		String[] strs = malString.split(":");
		for (String s : strs) {
			malset.add(s);
		}
	}

	private void matchPrintMal(String suspStr) {
		for (String malnode : this.malSRC) {
			if (suspStr.contains(malnode)) {
				report_writer.println("REPORT malNode matched:: EventStr: " + suspStr);
			}
		}

		for (String malnode : this.malSINK) {
			if (suspStr.contains(malnode)) {
				report_writer.println("REPORT malNode matched:: EventStr: " + suspStr);
			}
		}
	}

	private String convertPrintCUUID(UUID uuid, Object convertedRecord, String className, boolean print) {
		CUUID cuuid = new CUUID(uuid);
		String detailStr = className + "::" + cuuid.getHexUUID() + "::" + convertedRecord.toString();
		// print = false;
		if (print)
			System.out.println("new " + detailStr);
		return detailStr;
	}

	private void processFile(String srcsinkUUID, String eventUUID, SimpleEdge edge) {
		// srcsink -> event
		if (!srcsink2events.containsKey(srcsinkUUID)) {
			srcsink2events.put(srcsinkUUID, new ArrayList<String>());
		}
		srcsink2events.get(srcsinkUUID).add(eventUUID);
		// event -> srcsink
		event2srcsink.put(eventUUID, srcsinkUUID);

		if (this.pendingSrcSinkObject.containsKey(srcsinkUUID)) {
			if (this.suspEventNodes.containsKey(eventUUID)) {
				String fileDetailStr = this.pendingSrcSinkObject.get(srcsinkUUID);
				String eventDetailStr = this.suspEventNodes.get(event2srcsink.get(srcsinkUUID));
				this.sensitiveSrcSink.put(srcsinkUUID, fileDetailStr);
				report_writer.println("From fileDetailStr:" + fileDetailStr + "from Event:"
						+ eventDetailStr);
				matchPrintMal(fileDetailStr);
				if (eventDetailStr!=null)
					matchPrintMal(eventDetailStr);
			}
			this.pendingSrcSinkObject.remove(eventUUID);
		}
	}

	private void processSrcSink(String srcsinkUUID, String eventUUID, SimpleEdge edge) {
		// srcsink -> event
		if (!this.srcsink2events.containsKey(srcsinkUUID)) {
			this.srcsink2events.put(srcsinkUUID, new ArrayList<String>());
		}
		this.srcsink2events.get(srcsinkUUID).add(eventUUID);

		// event -> srcsink
		this.event2srcsink.put(eventUUID, srcsinkUUID);

		if (this.pendingSrcSinkObject.containsKey(srcsinkUUID)) {
			if (this.suspEventNodes.containsKey(eventUUID)) {
				String SrcSinkDetailStr = this.pendingSrcSinkObject.get(srcsinkUUID);
				String suspEventStr = this.suspEventNodes.get(event2srcsink.get(srcsinkUUID));
				this.sensitiveSrcSink.put(srcsinkUUID, SrcSinkDetailStr);
				report_writer.println("From SrcSinkDetail:" + SrcSinkDetailStr + "from Event:"
						+ suspEventStr);
				matchPrintMal(this.sensitiveSrcSink.get(srcsinkUUID));
				if (suspEventStr!=null)
					matchPrintMal(this.sensitiveSrcSink.get(suspEventStr));
			}
			this.pendingSrcSinkObject.remove(eventUUID);
		}

		if (this.sensitiveSrcSink.containsKey(srcsinkUUID)) {
			report_writer.println("SrcSink:: " + this.sensitiveSrcSink.get(srcsinkUUID) + " effected by Event:: "
					+ eventUUID + " Details::" + edge.get("timestamp"));
			matchPrintMal(this.sensitiveSrcSink.get(srcsinkUUID));
		}
	}

	private void writeReports(PrintWriter report_writer, String reportType) {
		Set<String> suspeventUUIDs = this.suspEventNodes.keySet();
		Set<String> senSrcSinkUUIDs = this.sensitiveSrcSink.keySet();

		System.out.println("Reporting========================" + reportType + "======================== TimeStamp:"
				+ this.currTimeStamp);
		report_writer.println("Reporting========================" + reportType + "======================== TimeStamp:"
				+ this.currTimeStamp);

		MaliciousPoint kafkaJsonOuput = new MaliciousPoint();

		// System.out.println("TIMESTAMP:" + currTimeStamp);
		kafkaJsonOuput.timestamp = currTimeStamp;
		for (String subjUUID : this.blkSubjectNodes.keySet()) {
			String subjStr = this.blkSubjectNodes.get(subjUUID);

			// System.out.println("ORIGIN:" +
			// "Marple::IBM-NWU-Analytic-ClearScope::EventScore");
			// System.out.println("UUID Subject Process:" + subjUUID);
			// System.out.println("Evidence:" + subjStr);

			// json format ouput
			kafkaJsonOuput.orign = "Marple::IBM-NWU-Analytic-ClearScope::EventScore";
			kafkaJsonOuput.UUID = subjUUID.toString();
			kafkaJsonOuput.addEvidence("Subject", subjStr);

			ArrayList<String> eventUUIDs = subject2events.get(subjUUID);
			int[] eventStatArray = new int[] { 0, 0, 0, 0 }; // EVENT_APP_UNKNOWN,
																// 0.04
			// EVENT_READ, 0.03
			// EVENT_WRITE, 0.08
			// EVENT_other 0.03
			int[] srcsinkStatArray = new int[] { 0, 0, 0, 0 }; // SOURCE_GPS,
			// 0.03
			// SOURCE_CAMERA, 0.05
			// NetFlowObject, 0.08
			// other 0.04
			double score = 0.65;
			double prescore = 0.68;
			if (blkSubjectScore.containsKey(subjUUID)) {
				prescore = blkSubjectScore.get(subjUUID);
			}
			if (eventUUIDs != null) {
				for (String eUUID : eventUUIDs) {
					if (suspeventUUIDs.contains(eUUID)) {
						// System.out.println(" suspEvent" +
						// suspEventNodes.get(eUUID));
						String eventStr = suspEventNodes.get(eUUID);
						if (eventStr.contains("EVENT_APP_UNKNOWN")) {
							eventStatArray[0]++;
						} else if (eventStr.contains("EVENT_READ")) {
							eventStatArray[1]++;
						} else if (eventStr.contains("EVENT_WRITE")) {
							eventStatArray[2]++;
						} else {
							System.out.println(" suspEventUnknownType:" + eventStr);
							kafkaJsonOuput.addEvidence("suspEventUnknownType", eventStr);
							eventStatArray[3]++;
						}
						// System.out.println(" REPORT: Suspicious Event::" +
						// eventStr + "From::" + subjUUID);
						report_writer.println("   REPORT: Suspicious Event::" + eventStr + "From::" + subjUUID);

					}

					if (this.event2srcsink.containsKey(eUUID)) {
						String sUUID = event2srcsink.get(eUUID);
						if (sUUID != null && sensitiveSrcSink.containsKey(sUUID)) {
							String senSrcSinkStr = this.sensitiveSrcSink.get(sUUID);
							if (senSrcSinkStr.contains("SOURCE_GPS")) {
								srcsinkStatArray[0]++;
							} else if (senSrcSinkStr.contains("SOURCE_CAMERA")) {
								srcsinkStatArray[1]++;
							} else if (senSrcSinkStr.contains("NetFlowObject")) {
								srcsinkStatArray[2]++;
							} else {
								// System.out.println(" sensitiveSrcSink:" +
								// senSrcSinkStr);
								kafkaJsonOuput.addEvidence("sensitiveSrcSink", senSrcSinkStr);
								srcsinkStatArray[3]++;
							}
							// System.out.println(" REPORT: Suspicious
							// SrcSinkPoints:" + senSrcSinkStr+ "From::" +
							// subjUUID);
							report_writer.println(
									"         REPORT: Suspicious SrcSinkPoints:" + senSrcSinkStr + "From::" + subjUUID);

						}
					}
				}
				/*
				 * System.out.println(" eventStatArray" + eventStatArray[0] +
				 * ":" + eventStatArray[1] + ":" + eventStatArray[2] + ":" +
				 * eventStatArray[3]);
				 */
				kafkaJsonOuput.addEvidence("eventStatArray", eventStatArray[0] + ":" + eventStatArray[1] + ":"
						+ eventStatArray[2] + ":" + eventStatArray[3]);
				/*
				 * System.out.println("   sensitiveSrcSink" +
				 * srcsinkStatArray[0] + ":" + srcsinkStatArray[1] + ":" +
				 * srcsinkStatArray[2] + ":" + srcsinkStatArray[3]);
				 */
				kafkaJsonOuput.addEvidence("sensitiveSrcSink", srcsinkStatArray[0] + ":" + srcsinkStatArray[1] + ":"
						+ srcsinkStatArray[2] + ":" + srcsinkStatArray[3]);

				if (srcsinkStatArray[0] > 0) {
					score += 0.03 + srcsinkStatArray[0] / 10000;
				}
				if (srcsinkStatArray[1] > 0) {
					score += 0.05 + srcsinkStatArray[1] / 10000;
				}
				if (srcsinkStatArray[2] > 0) {
					score += 0.08 + srcsinkStatArray[2] / 10000;
				}
				if (srcsinkStatArray[3] > 0) {
					score += 0.04 + srcsinkStatArray[3] / 10000;
				}

				if (eventStatArray[0] > 0) {
					score += 0.04 + eventStatArray[0] / 10000;
				}
				if (eventStatArray[1] > 0) {
					score += 0.03 + eventStatArray[1] / 10000;
				}
				if (eventStatArray[2] > 0) {
					score += 0.08 + eventStatArray[2] / 10000;
				}
				if (eventStatArray[3] > 0) {
					score += 0.03 + eventStatArray[3] / 10000;
				}

				if (score > 1.0)
					score = 1.0;
				if (prescore < score) {
					blkSubjectScore.put(subjUUID, score);
				}
			}

			if (score > prescore) {
				// System.out.println(" Score:" + score);
				kafkaJsonOuput.score = score;
			} else {
				// System.out.println(" Score:" + prescore);
				kafkaJsonOuput.score = prescore;
			}

			this.producer.run(kafkaJsonOuput.outputRecordAsJson());
			report_writer.flush();
		}
	}

	private void printProvGraph(PrintWriter report_writer) {
		if (this.provGraph.edgeSet() != null) {
			for (DefaultEdge e : this.provGraph.edgeSet()) {
				// System.out.println("Reporting Provs edge::" + e);
				report_writer.print("Provs edge::" + e + "\n");
			}
		}
		if (this.provGraph.vertexSet() != null) {
			for (String v : this.provGraph.vertexSet()) {
				// System.out.println("Reporting Provs edge::" + e);
				report_writer.print("Provs node::" + v + "\n");
			}
		}
		report_writer.print("Reporting Provs edge Total Number:: " + this.provGraph.edgeSet().size() + "\n");
	}

	private void getProvPathforSubj() {

		// REPORT fileDetailStr
		// Details:FileObject::bf1661819b80c3942cc613648acf8962::
		// {"uuid": [-65, 22, 97, -127, -101, -128, -61, -108, 44, -58, 19, 100,
		// -118, -49, -119, 98], "baseObject": {"source":
		// "SOURCE_ANDROID_JAVA_CLEARSCOPE", "permission": null,
		// "lastTimestampMicros": null, "properties": null}, "url":
		// "/storage/emulated/0/gather.txt [-rw-rw----]", "isPipe": false,
		// "version": 1, "size": null}

		Set<String> subjs = this.blkSubjectNodes.keySet();
		// Set<String> subjsW = this.whtSubjectNodes.keySet();
		// "/data/data" "/storage" "/data/local/tmp" "/data/system"
		// "/dev/block/" "/proc"
		// System.out.println("PATHH start:\n");

		Iterator<String> itra = subjs.iterator();
		Iterator<String> itrb = subjs.iterator();
		while (itra.hasNext()) {
			String subja = itra.next();
			while (itrb.hasNext()) {
				String subjb = itrb.next();
				// Sysxtem.out.println("PATHH subja + subjb:"+ subja + ":" +
				// subjb +"\n");
				if (subja != subjb) {
					ArrayList<String> outputTags = this.subj2outputTags.get(subja);
					ArrayList<String> inputTags = this.subj2inputTags.get(subjb);
					if (inputTags != null && outputTags != null) {
						System.out.println("PATHH inputTags + outputTags:" + inputTags + ":" + outputTags + "\n");
						for (String oTag : outputTags) {
							for (String iTag : inputTags) {
								if (iTag != null && oTag != null && this.provGraph.containsVertex(iTag)
										&& this.provGraph.containsVertex(oTag)) {
									try {
										System.out.println("PATHH:" + "DijkstraShortestPath" + oTag + ":" + iTag);
										// System.out.println("PATHH:" +
										// DijkstraShortestPath.findPathBetween(this.provGraph,
										// oTag, iTag));

									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						}
					}
				}
			}
		}
		/*
		 * Iterator<String> itrc = subjs.iterator(); while (itrc.hasNext()) {
		 * String subjc = itrc.next(); ArrayList<String> outputTags =
		 * this.subj2outputTags.get(subjc); ArrayList<String> inputTags =
		 * this.subj2inputTags.get(subjc); for (String iTag : inputTags) { if
		 * (iTag != null) { try { //System.out.println("PATHH:" +
		 * DijkstraShortestPath.findPathBetween(this.provGraph, oTag, iTag)); }
		 * catch (Exception e) { e.printStackTrace(); }} } }
		 */

	}

	private String printEventParamDetails(Value v) {
		ValueDataType dataType = v.getValueDataType();
		String paraString = "";
		if (!v.isNull) {
			// parms.get(j).getValueBytes().
			ByteBuffer data;
			data = (ByteBuffer) v.getValueBytes();
			switch (dataType) {
			case VALUE_DATA_TYPE_INT:
				// System.out.print("INT: ");
				paraString += "INT: ";
				while (data.hasRemaining()) {
					// System.out.print(data.getInt());
					paraString += data.getInt();
				}
				// System.out.println("");
				break;

			case VALUE_DATA_TYPE_CHAR:
				paraString += ("CHAR: ");
				try {
					byte[] bytes = null;
					while (data.hasRemaining()) {
						bytes = new byte[data.remaining()];
						data.get(bytes);
					}
					String string = "";
					if (bytes != null) {
						string = new String(bytes, "Unicode");
						paraString += (string.replaceAll("\0", ""));
					}
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;

			case VALUE_DATA_TYPE_BOOL:
				paraString += "BOOL: ";
				break;

			case VALUE_DATA_TYPE_BYTE:
				paraString += "BYTE: ";
				while (data.hasRemaining()) {
					paraString += data.get();
				}
				// System.out.println("");
				break;

			case VALUE_DATA_TYPE_DOUBLE:
				paraString += ("DOUBLE: ");
				while (data.hasRemaining()) {
					paraString += (data.getDouble());
				}
				// System.out.println("");
				break;

			case VALUE_DATA_TYPE_FLOAT:
				paraString += ("FLOAT: ");
				while (data.hasRemaining()) {
					paraString += (data.getFloat());
				}
				// System.out.println("");
				break;

			case VALUE_DATA_TYPE_LONG:
				paraString += ("LONG: ");
				while (data.hasRemaining()) {
					paraString += (data.getLong());
				}
				// System.out.println("");
				break;

			case VALUE_DATA_TYPE_SHORT:
				paraString += ("SHORT: ");
				while (data.hasRemaining()) {
					paraString += data.getShort();
				}
				// System.out.println("");
				break;

			default:
				break;
			}
		}
		return paraString + "";
	}

	private String printEventInOutDetails(Event event) {
		UUID eUUID = event.getUuid();
		String eventUUID = convertPrintCUUID(eUUID, event, "Event", false);
		String EventIOtype = "";
		if (event.getParameters() != null){ //&& this.suspEventNodes.containsKey(eventUUID)) {
			int lens = event.getParameters().size();
			for (Value v : event.getParameters()) {
				// System.out.println(event.toString() + " REPORT ParamDetail::"
				// + printEventParamDetails(v));
				switch (v.getType()) {
				case VALUE_TYPE_IN:
					if (v.getTag() != null) {
						// "doc" : "* The value's tag expression describing its
						// provenance (Optional)\n * Since value could be an
						// array, the tag can use run length encoding if needed.
						// The format of the array is:\n * {<numElements:int>,
						// <tagId:int>}*\n * For example, to assign a tag 0
						// (unknown) to elements 0-3, 1 to elements 4-7 and 2 to
						// elements 8-15 of\n * an int[16] value, this would be
						// represented using the following tag array\n * {4, 0,
						// 4, 1, 8, 2} meaning the first 4 elements have tag 0,
						// next 4 have tag 1, next 8 have tag 2\n * Note that 4
						// elements of the array correspond to 16 bytes in the
						// valueBytes buffer\n * Note that tagId had to be
						// defined/emitted earlier (see ProvenanceTagNode)\n *
						// before it can be assigned to a value",
						for (int i = 1; i < v.getTag().size(); i = i + 2) { // every
																			// second
																			// value
																			// is
																			// the
																			// tagId
							String tag = v.getTag().get(i).toString();
							
							matchPrintMal(tag);
							/*
							report_writer.println("REPORT Subj::" + this.event2subject.get(eventUUID) + " Susp Event::"
									+ eventUUID + " Input Tags::" + tag);
							*/
							String subjectUUID = this.event2subject.get(eventUUID);
							if (!EventIOtype.contains("input")) {
								EventIOtype += ":input";
							}
							if (!subj2inputTags.containsKey(subjectUUID))
								subj2inputTags.put(subjectUUID, new ArrayList<String>());
							subj2inputTags.get(subjectUUID).add(tag);
							// }
						}
					}
					break;
				case VALUE_TYPE_OUT:
					if (v.getTag() != null) {
						for (int i = 1; i < v.getTag().size(); i = i + 2) {
							String tag = v.getTag().get(i).toString();
							// if (this.provGraph.containsVertex(tag)) {
							matchPrintMal(tag);
							/*
							report_writer.println("REPORT Subj:" + this.event2subject.get(eventUUID) + " Susp Event:"
									+ eventUUID + " Output Tags:" + tag);
							*/
							String subjectUUID = this.event2subject.get(eventUUID);
							if (!EventIOtype.contains("output")) {
								EventIOtype += ":output";
							}
							if (!subj2outputTags.containsKey(subjectUUID))
								subj2outputTags.put(subjectUUID, new ArrayList<String>());
							subj2outputTags.get(subjectUUID).add(tag);
							// }
						}
					}
					break;
				case VALUE_TYPE_INOUT:
					System.out.println("REPORT Found param VALUE_TYPE_INOUT:" + v.getType());
					if (!EventIOtype.contains("ioput")) {
						EventIOtype += ":ioput";
					}
					break;
				default:

					break;
				}
			}
		}
		return EventIOtype;
	}

	private void readConfiguration(String configFile) throws IOException {
		// load config file
		Properties props = new Properties();
		FileInputStream in = new FileInputStream(configFile);
		props.load(in);

		// set properties
		this.consumerServer = props.getProperty("ConsumerServer");
		this.producerServer = props.getProperty("ProducerServer");
		this.readTopic = props.getProperty("ReadTopic");
		this.writeTopic = props.getProperty("WriteTopic");
		this.exiton0 = Boolean.valueOf(props.getProperty("Exiton0"));
		this.groupId = props.getProperty("GroupId");
		this.schemaFilename = props.getProperty("SchemaFilename");

		// read global settings from system environments, if the corresponding
		// system env variables are set, use this value instead of the old one
		String producerServerEnv = System.getenv("PRODUCERSERVER");
		String writeTopicEnv = System.getenv("WRITETOPIC");
		String consumerServerEnv = System.getenv("CONSUMERSERVER");
		String readTopocEnv = System.getenv("READTOPIC");

		if (producerServerEnv != null)
			this.producerServer = producerServerEnv;
		if (writeTopicEnv != null)
			this.writeTopic = writeTopicEnv;
		if (consumerServerEnv != null)
			this.consumerServer = consumerServerEnv;
		if (readTopocEnv != null)
			this.readTopic = readTopocEnv;
	}

	/**
	 * load whitelist of app
	 * 
	 * @throws IOException
	 */
	private void readWhiteapplist(String whiteapplist) throws IOException {
		File file = new File(whiteapplist);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String tempString = null;
		while ((tempString = reader.readLine()) != null) {
			if (!tempString.contains("#")) {
				System.out.println("WHITEAPP added:" + tempString);
				this.WHITEAPP.add(tempString.trim());
			}
		}
		reader.close();
	}

	/**
	 * configure Kafka consumer
	 */
	private Properties createConsumerProps() {
		Properties props = new Properties();

		props.put("bootstrap.servers", this.consumerServer);
		props.put("group.id", groupId);

		props.put("session.timeout.ms", "30000");
		props.put("auto.commit.interval.ms", "10000");

		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringDeserializer");
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				com.bbn.tc.schema.serialization.kafka.KafkaAvroGenericDeserializer.class);
		props.put(AvroConfig.SCHEMA_READER_FILE, schemaFilename);
		props.put(AvroConfig.SCHEMA_WRITER_FILE, schemaFilename);
		props.put(AvroConfig.SCHEMA_SERDE_IS_SPECIFIC, true);
		props.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

		return props;
	}

	/**
	 * take different actions for the corresponding datum Type
	 */
	private void processDatum(Object datum) {

		// extract the class name, such as "com.bbn.tc.schema.avro.SimpleEdge"
		// -> "SimpleEdge"
		// //System.out.println(datum);

		String className = datum.getClass().getName().substring(23);
		String eventUUID;
		UUID eUUID;
		String subjectUUID;
		UUID subUUID;
		String srcsinkUUID;
		UUID ssUUID;

		String eventDetailStr;
		String eventParamValue;
		String eventIOType;
		String suspeventDetailStr;
		String srcSinkDetailStr;
		String subjDetailStr;
		// List<Value> parms = new ArrayList<Value>();
		CUUID cuuid;
		Event event;
		SrcSinkObject srcSinkObject;
		Object eventdatum = null;

		switch (className) {
		case "Subject":
			Subject subject = (Subject) datum;
			subUUID = subject.getUuid();

			subjDetailStr = convertPrintCUUID(subUUID, subject, className, false);
			subjectUUID = subjDetailStr.split("::")[1];

			matchPrintMal(subjDetailStr);
		
			// whether in the white app list
			String appname = subject.getCmdLine().toString();
			if (this.WHITEAPP.contains(appname)) {
				System.out.println("White::appname: " + appname + " subjectDetails:: " + subjDetailStr);
				this.whtSubjectNodes.put(subjectUUID, subjDetailStr);
				this.matchedWhiteApp.add(subjectUUID);
				// //System.out.println(subject.getCmdLine() + "in the
				// whitelist
				// app");
			} else {
				this.blkSubjectNodes.put(subjectUUID, subjDetailStr);
				System.out.println("Suspicoius::appname: " + appname + " subjectDetails:: " + subjDetailStr);
			}
			// subject.getClass().toString() + ":" + subject.getPid().toString()
			// + ":" + subject.getPpid()
			// + ":" + subject.getStartTimestampMicros().toString() + ":" + ;
			break;

		case "Event":
			// System.out.println("EVENT_printingbefore0:" + datum.toString());
			event = (Event) datum;
			eUUID = event.getUuid();
			eventDetailStr = convertPrintCUUID(eUUID, event, className, false);
			eventUUID = eventDetailStr.split("::")[1];
			// System.out.println("eventUUID::" + eventUUID);
			// System.out.println("EVENT_printing:" + event.toString());
			// System.out.println("EVENT_printing:" + eventDetailStr);
			/*
			 * if (UUIDstr.length()==32) System.out.println("UUIDstr :" +
			 * UUIDstr); else System.out.println("UUIDstr not 32:" + UUIDstr);
			 */
			// HashSet<String> parameters = new HashSet<String>();
			matchPrintMal(eventDetailStr);
			
			long timestamp = event.getTimestampMicros();
			if (currTimeStamp < timestamp)
				currTimeStamp = timestamp;
			// String name = event.getName().toString();

			List<Value> params = event.getParameters();
			eventParamValue = "";
			if (params != null) {
				eventParamValue = "eventParam::";
				for (Value v : params) {
					String type = printEventParamDetails(v);
					eventParamValue = eventParamValue + ' ' + type + ' ';
				}
				// System.out.println(eventParamValue+"");
			}

			eventIOType = printEventInOutDetails(event);
			eventDetailStr = eventParamValue + ' ' + eventDetailStr + ' ' + eventIOType;
			// System.out.println("Print-eventDetailStr::" + eventDetailStr);

			if (this.event2subject.containsKey(eventUUID)
					&& this.blkSubjectNodes.containsKey(event2subject.get(eventUUID))) {
				this.suspEventNodes.put(eventUUID, eventDetailStr);
				// String subjectstr =
				// this.blkSubjectNodes.get(event2subject.get(eventUUID));
				// if (eventParamValue.length()>15){
				// System.out.println("EVENT_printing:" + eventDetailStr);
				// System.out.println(eventParamValue + " "+
				// eventIOType+"EVENT_printing REPORT suspeventDetailStr:: " +
				// eventDetailStr + "from Subject:: " +
				// event2subject.get(eventUUID).toString() + "");
				report_writer.println("REPORT suspeventDetailStr:: " + eventDetailStr + " from Subject:: "
						+ event2subject.get(eventUUID).toString() + "");
				// }
				// System.out.println("from Subject:" +
				// event2subject.get(eventUUID));
			} else {
				// System.out.println("EVENT_printingbefore:" +
				// eventdatum.toString());
				this.pendingEventNodes.put(eventUUID, eventDetailStr);
				// System.out.println("EVENT_printingafter:" +
				// pendingEventNodes.get(eventUUID).toString());
			}
			// this.pendingEventNodes.put(eventUUID, className + ":" +
			// event.toString());
			// process Values for tag in and out
			// String eventIOtype = printEventInOutDetails(event);

			// String eventIOType = printEventInOutDetails(event);
			// if (eventParamValue.length()>15){
			// report_writer.println(eventParamValue +" REPORT EventType:: " +
			// eventDetailStr + "from Subject:: " + event2subject.get(eventUUID)
			// + "");
			// }
			break;

		case "FileObject":
			FileObject fileobj = (FileObject) datum;
			ssUUID = fileobj.getUuid();
			String fileDetailStr = convertPrintCUUID(ssUUID, fileobj, className, false);
			srcsinkUUID = fileDetailStr.split("::")[1];

			// System.out.println("propertiesPrint::" +
			// fileobj.get("properties"));
			matchPrintMal(fileDetailStr);
			
			if (!srcsink2events.isEmpty() && srcsink2events.size() > 0) {
				ArrayList<String> events = srcsink2events.get(srcsinkUUID);
				// //System.out.println("events Details" +
				// events.toString());
				if (events != null) {
					for (String euuid : events) {
						if (this.suspEventNodes.containsKey(euuid)) {
							this.sensitiveSrcSink.put(srcsinkUUID, fileDetailStr);
							// System.out.println("from Event:" +
							// suspEventNodes.get(euuid));
							report_writer.println("REPORT susp fileDetailStr Details:" + fileDetailStr
									+ "from susp event:" + suspEventNodes.get(euuid));
						}
					}
				}
			} else // suggested by Runqing
				this.pendingSrcSinkObject.put(srcsinkUUID, fileDetailStr);
			// this.sensitiveSrcSink.put(srcsinkUUID, fileDetailStr);
			report_writer.println("REPORT fileDetailStr Details:" + fileDetailStr);
			this.sensitiveFile.put(srcsinkUUID, fileDetailStr);
			// this.sensitiveSrcSink.put(srcsinkUUID, fileDetailStr);
			// System.out.println("REPORT fileDetailStr Details" +
			// fileDetailStr);
			break;

		case "NetFlowObject":
			NetFlowObject netflow = (NetFlowObject) datum;
			ssUUID = netflow.getUuid();
			String netflowDetailStr = convertPrintCUUID(ssUUID, netflow, className, false);
			srcsinkUUID = netflowDetailStr.split("::")[1];

			matchPrintMal(netflowDetailStr);

			if (!srcsink2events.isEmpty() && srcsink2events.size() > 0) {
				ArrayList<String> events = srcsink2events.get(srcsinkUUID);
				// //System.out.println("events Details" +
				// events.toString());

				if (events != null) {
					for (String euuid : events) {
						if (this.suspEventNodes.containsKey(euuid)) {
							this.sensitiveSrcSink.put(srcsinkUUID, netflowDetailStr);
							// System.out.println("from Event:" +
							// suspEventNodes.get(euuid));
							report_writer.println("REPORT susp NetFlowObject Details" + netflowDetailStr
									+ "from susp event:" + suspEventNodes.get(euuid));
						}
					}
				}
			} else
				this.pendingSrcSinkObject.put(srcsinkUUID, netflowDetailStr);

			this.sensitiveSrcSink.put(srcsinkUUID, netflowDetailStr);
			report_writer.println("REPORT NetFlowObject Details:" + netflowDetailStr);
			break;

		case "SrcSinkObject":
			srcSinkObject = (SrcSinkObject) datum;
			ssUUID = srcSinkObject.getUuid();
			srcSinkDetailStr = convertPrintCUUID(ssUUID, srcSinkObject, className, false);
			srcsinkUUID = srcSinkDetailStr.split("::")[1];
			
			matchPrintMal(srcSinkDetailStr);
			
			switch (srcSinkObject.getType()) {
			case SOURCE_UNKNOWN:

				// srcSinkObject.getClass()

				if (!srcsink2events.isEmpty() && srcsink2events.size() > 0) {
					ArrayList<String> events = srcsink2events.get(srcsinkUUID);
					// //System.out.println("events Details" +
					// events.toString());
					if (events != null) {
						for (String euuid : events) {
							if (this.suspEventNodes.containsKey(euuid)) {
								this.sensitiveSrcSink.put(srcsinkUUID, srcSinkDetailStr);
								report_writer.println("sensitiveSrcSink:" + srcSinkDetailStr + "from Event:"
										+ suspEventNodes.get(euuid));
								// System.out.println("
								// System.out.println("srcSink Details" +
								// srcSinkObject.toString());
							}
						}
					}
				} else
					this.pendingSrcSinkObject.put(srcsinkUUID, srcSinkDetailStr);
				break;

			default:
				this.sensitiveSrcSink.put(srcsinkUUID, srcSinkDetailStr);
				report_writer.println("from sensitiveSrcSinkSpecial===" + srcSinkDetailStr);

				break;
			}

			break;

		case "SimpleEdge":
			SimpleEdge edge = (SimpleEdge) datum;

			switch (edge.getType()) {
			case EDGE_EVENT_ISGENERATEDBY_SUBJECT:
				subUUID = edge.getToUuid();
				eUUID = edge.getFromUuid();

				cuuid = new CUUID(subUUID);
				subjectUUID = cuuid.getHexUUID();
				cuuid = new CUUID(eUUID);
				eventUUID = cuuid.getHexUUID();

				// subject -> events
				if (!subject2events.containsKey(subjectUUID))
					subject2events.put(subjectUUID, new ArrayList<String>());
				subject2events.get(subjectUUID).add(eventUUID);

				// System.out.println("subjectevent" + subjectUUID + ' ' +
				// eventUUID);
				// event -> subject
				event2subject.put(eventUUID, subjectUUID);
				if (this.pendingEventNodes.containsKey(eventUUID)) {
					// System.out.println("containsE::" + eventUUID);
					if (this.blkSubjectNodes.containsKey(subjectUUID)) {
						// Event suspEvent = (Event)
						// this.pendingEventNodes.get(eventUUID);
						// suspeventDetailStr =
						// convertPrintCUUID(suspEvent.getUuid(), suspEvent,
						// "EVENT", false);
						suspeventDetailStr = this.pendingEventNodes.get(eventUUID);
						
						matchPrintMal(suspeventDetailStr);

						this.suspEventNodes.put(eventUUID, suspeventDetailStr);
						// String subjectstr =
						// this.blkSubjectNodes.get(event2subject.get(eventUUID));
						// //System.out.println("FromPenndingEventType:" +
						// event.getType().toString());
						// //System.out.println("from Subject:" +
						// subject.getCmdLine());
						// //System.out.println("Event Details" +
						// event.toString());

						// System.out.println("EVENT_printing:before" +
						// suspEvent.toString());
						/*
						 * eventParamValue = ""; List<Value> pms =
						 * suspEvent.getParameters(); //params =
						 * event.getParameters();
						 * 
						 * if (pms != null) { eventParamValue = "eventParam::";
						 * for (Value v : pms) { String type =
						 * printEventParamDetails(v); eventParamValue =
						 * eventParamValue + ' ' + type + ' '; }
						 * //System.out.println(eventParamValue+""); }
						 * eventIOType = printEventInOutDetails(suspEvent);
						 */
						// System.out.println("EVENT_printing:" +
						// suspeventDetailStr);
						// System.out.println(eventParamValue + ' '+
						// eventIOType+" REPORT suspeventDetailStr:: " +
						// suspeventDetailStr + "from Subject:: " +
						// event2subject.get(eventUUID).toString() + "");
						report_writer.println("REPORT suspeventDetailStr:: " + suspeventDetailStr + " from Subject:: "
								+ event2subject.get(eventUUID) + "");
					} else if (this.whtSubjectNodes.containsKey(subjectUUID)) {
						// System.out.println("EventTypeRemoved:: " +
						// this.pendingEventNodes.get(eventUUID) + "from
						// Subject:: " + event2subject.get(eventUUID));
						pendingEventNodes.remove(eventUUID);
						event2subject.remove(eventUUID);
					}
					// this.pendingEventNodes.remove(eventUUID);
				}
				break;

			// case EDGE_EVENT_AFFECTS_NETFLOW;
			case EDGE_EVENT_AFFECTS_FILE:
				ssUUID = edge.getToUuid();
				eUUID = edge.getFromUuid();
				cuuid = new CUUID(ssUUID);
				srcsinkUUID = cuuid.getHexUUID();
				cuuid = new CUUID(eUUID);
				eventUUID = cuuid.getHexUUID();

				processFile(srcsinkUUID, eventUUID, edge);
				break;
			// case EDGE_EVENT_AFFECTS_NETFLOW:
			// break;
			case EDGE_FILE_AFFECTS_EVENT:
				eUUID = edge.getToUuid();
				ssUUID = edge.getFromUuid();
				cuuid = new CUUID(ssUUID);
				srcsinkUUID = cuuid.getHexUUID();
				cuuid = new CUUID(eUUID);
				eventUUID = cuuid.getHexUUID();

				processFile(srcsinkUUID, eventUUID, edge);
				break;

			case EDGE_SRCSINK_AFFECTS_EVENT:
				ssUUID = edge.getFromUuid();
				eUUID = edge.getToUuid();

				cuuid = new CUUID(ssUUID);
				srcsinkUUID = cuuid.getHexUUID();
				cuuid = new CUUID(eUUID);
				eventUUID = cuuid.getHexUUID();

				processSrcSink(srcsinkUUID, eventUUID, edge);
				break;

			case EDGE_EVENT_AFFECTS_SRCSINK:
				eUUID = edge.getFromUuid();
				ssUUID = edge.getToUuid();

				cuuid = new CUUID(ssUUID);
				srcsinkUUID = cuuid.getHexUUID();
				cuuid = new CUUID(eUUID);
				eventUUID = cuuid.getHexUUID();

				processSrcSink(srcsinkUUID, eventUUID, edge);

				break;
			// case EDGE_EVENT_AFFECTS_NETFLOW:
			// break;
			default:
				System.out.println("CaseEdgeType Not Processed " + edge.getType().toString());
				break;
			}

			break;

		case "ProvenanceTagNode":
			// ArrayList<Object> srcTagIdList;
			ProvenanceTagNode provenanceTagNode = (ProvenanceTagNode) datum;
			Integer targetTagId = provenanceTagNode.getTagId();
			
			String parentTagId = "";
			if (provenanceTagNode.getValue().toString().contains(",")) {
				UUID inputUUID = (UUID) provenanceTagNode.getValue();
				CUUID inputcuuid = new CUUID(inputUUID);
				parentTagId = inputcuuid.getHexUUID();
			}
			
			this.provGraph.addVertex(targetTagId.toString());
			if (provenanceTagNode.getChildren() != null) {
				for (ProvenanceTagNode childprovnode : provenanceTagNode.getChildren()) {
					// srcTagIdList.add(childprovnode.getValue()); // UUID or

					String srcTagId = "";
					if (childprovnode.getValue().toString().contains(",")) {
						UUID inputUUID = (UUID) childprovnode.getValue();
						CUUID inputcuuid = new CUUID(inputUUID);
						srcTagId = inputcuuid.getHexUUID();
					} else
						srcTagId = childprovnode.getValue().toString();

					this.provGraph.addVertex(srcTagId);
					// System.out.println("Provs Edge Added <= " +
					// childprovnode.getValue());
					this.provGraph.addEdge(targetTagId.toString(), srcTagId);
				}
			} 
			
			report_writer.println("provTagNode::parentTagId:"+parentTagId+"::" + provenanceTagNode.toString());

			
			break;
		// com.bbn.tc.schema.avro.Principal
		default:
			System.out.println("CaseType Not Processed " + datum.getClass().toString());
			break; // com.bbn.tc.schema.avro.Principal
					// EDGE_SUBJECT_HASLOCALPRINCIPAL
		}
	}

	/**
	 * constructor with default settings
	 */
	public KafkaAnalyzer() {
		this("config.properties", "whiteapp.list");
	}

	/**
	 * load configuration and init Kafka settings
	 */
	public KafkaAnalyzer(String configFile, String whiteapplist) {
		// load settings, if error, exit.
		try {
			readConfiguration(configFile);
			readWhiteapplist(whiteapplist);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}

		// System.out.println(this.WHITEAPP);

		// init kafka consumer and producer
		consumer = new KafkaConsumer(createConsumerProps());
		producer = new Producer(producerServer, groupId, writeTopic);
	}

	/**
	 * start analysis from Kafka
	 */
	public void start() {
		Schema schema = null;
		try {
			schema = new Schema.Parser().parse(new File(this.schemaFilename));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		DatumReader<TCCDMDatum> datumReader = new SpecificDatumReader<TCCDMDatum>(schema);

		TopicPartition partition0 = new TopicPartition(this.readTopic, 0);
		consumer.assign(Arrays.asList(partition0));
		Set<TopicPartition> partitions = consumer.assignment();
		for (TopicPartition pInfo : partitions) {
			consumer.seekToBeginning(pInfo);
		}

		// create reports dirs
		String log_random_id = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
		String report_dir_path = "/mnt/reports/reports-" + log_random_id + "/";
		File report_dir = new File(report_dir_path);
		report_dir.mkdirs();

		// output to report file
		File report_file = new File(report_dir_path + "summary.txt");
		try {
			report_writer = new PrintWriter(new FileOutputStream(report_file.getAbsolutePath()));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		addMalNodeToCheck(malSRC, malProvNodesSrc);
		addMalNodeToCheck(malSINK, malProvNodesSink);

		boolean printflag = false;
		int datumcounter = 0;
		int printgraph = 0;
		while (!this.exiton0) {

			// Polling...
			ConsumerRecords<String, GenericContainer> records = consumer.poll(50000);
			// Handle new records
			Iterator recIter = records.iterator();

			// loop every records
			while (recIter.hasNext()) {
				ConsumerRecord<String, GenericContainer> record = (ConsumerRecord<String, GenericContainer>) recIter
						.next();
				// GenericContainer ctr = record.value();
				TCCDMDatum CDMdatum = (TCCDMDatum) record.value();
				Object datum = CDMdatum.getDatum();
				// //System.out.println("=========start process
				// datum=========");

				processDatum(datum);
				datumcounter++;
				if (printgraph % 100000 == 0) {
					getProvPathforSubj();
				}

				if (printgraph > 2657010 && printflag == false) {// K-B3 3217090
																	// K-A
																	// 59158239
																	// S 2657010
					printProvGraph(report_writer);
					printflag = true;
				}

				if (printgraph % 1500000 == 0) {
					String reportType = "Periodical";
					// writeReports(report_writer, reportType);
				}
				printgraph++;

				if (datumcounter > 5000000) {
					String reportType = "Final";
					writeReports(report_writer, reportType);
					printProvGraph(report_writer);
					datumcounter = 0;
					if (true) {
						System.out.println("Refreshing........................");
						pendingEventNodes.clear();
						pendingSrcSinkObject.clear();
						event2srcsink.clear();
						// srcsink2events.clear();
						event2subject.clear();
						// subject2events.clear();
					}
				}
				CDMdatum = null;
				datum = null;
			}
		}
		/*
		 * String reportType = "FinalFinal"; writeReports(report_writer,
		 * reportType); printProvGraph(report_writer); getProvPathforSubj();
		 */
		report_writer.close();
	}

	/*
	 * read cdm data from avro file
	 */
	public void startFromAvroFile(String avroFilePath) throws IOException {
		// read reocrds from a avro file
		Schema schema = new Schema.Parser().parse(new File(this.schemaFilename));
		DatumReader<TCCDMDatum> datumReader = new SpecificDatumReader<TCCDMDatum>(schema);
		DataFileReader<TCCDMDatum> dataFileReader = null;
		dataFileReader = new DataFileReader(new File(avroFilePath), datumReader);
		TCCDMDatum transaction = null;

		// loop every records
		while (dataFileReader.hasNext()) {
			transaction = dataFileReader.next(transaction);
			processDatum(transaction.getDatum());
		}
	}
}

/*
 * while (printgraph > 1000) { System.out.println("Reporting ProveanceGraph");
 * for (DefaultEdge e : this.provGraph.edgeSet()) {
 * System.out.println("Reporting Edege:" + e); } printgraph++; }
 */

// ArrayList<String> parmStrings = getParmString(event);
/*
 * if (parms != null) { for (int j = 0; j < parms.size(); j++) { // if
 * (parmStrings.get(j).equals("null")) { //
 * System.out.println(Integer.toString(((ByteBuffer)parms.get(j).get
 * ("valueBytes")).getInt(), // 16)); //
 * System.out.println(Integer.toString(((ByteBuffer)parms.get(j).get
 * ("valueBytes")).capacity(), // 16)); if (parms.get(j) != null) { ByteBuffer
 * valueBytes_bbuffer = (ByteBuffer) parms.get(j).getValueBytes(); byte[]
 * valueBytes_bytes; if (valueBytes_bbuffer.hasRemaining()) { valueBytes_bytes =
 * new byte[valueBytes_bbuffer.remaining()]; valueBytes_bbuffer =
 * valueBytes_bbuffer.get(valueBytes_bytes); byte[] bytes =
 * valueBytes_bbuffer.array(); // String valueBytes = //
 * DatatypeConverter.printHexBinary(valueBytes_bytes);
 * 
 * // String s = new String(valueBytes, "US-ASCII");
 * parameters.add(bytes.toString());
 * 
 * } }
 * 
 * // System.out.printf("Events Parsed %s\n", valueBytes);
 * 
 * // } else { // parameters.add(parmStrings.get(j)); //
 * System.out.println("Events Parsed" + parmStrings.get(j)); // } } } for
 * (String parmstr : parameters) { System.out.println("Events Parsed:" +
 * parmstr); }
 */